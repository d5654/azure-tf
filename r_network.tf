###############
#   Network   #
###############
resource "azurerm_virtual_network" "main_vnet" {
  count               = length(var.caf_naming)
  name                = azurecaf_name.caf_vnet[count.index].result
  address_space       = var.vnet_address_space
  location            = var.location
  resource_group_name = azurerm_resource_group.main_rg[count.index].name

  tags = var.default_tags
}

resource "azurerm_subnet" "aci_snet" {
  count                = length(var.caf_naming)
  name                 = azurecaf_name.caf_snet1[count.index].result
  resource_group_name  = azurerm_resource_group.main_rg[count.index].name
  virtual_network_name = azurerm_virtual_network.main_vnet[count.index].name
  address_prefixes     = [cidrsubnet(azurerm_virtual_network.main_vnet[count.index].address_space[0], 4, 4)]
  service_endpoints    = var.aci_snet_svc_endpoints

  delegation {
    name = "container-group-delegation"
    service_delegation {
      name = "Microsoft.ContainerInstance/containerGroups"
      actions = [
        "Microsoft.Network/virtualNetworks/subnets/action",
      ]
    }
  }
}

resource "azurerm_subnet" "appgw_snet" {
  count                = length(var.caf_naming)
  name                 = azurecaf_name.caf_snet2[count.index].result
  resource_group_name  = azurerm_resource_group.main_rg[count.index].name
  virtual_network_name = azurerm_virtual_network.main_vnet[count.index].name
  address_prefixes     = [cidrsubnet(azurerm_virtual_network.main_vnet[count.index].address_space[0], 4, 1)]
}

resource "azurerm_public_ip" "appgw_pip" {
  count    = length(var.caf_naming)
  name     = azurecaf_name.caf_pip[count.index].result
  location = var.location

  resource_group_name = azurerm_resource_group.main_rg[count.index].name
  allocation_method   = var.allocation_method
  domain_name_label   = var.domain_name_label
  sku                 = var.sku_pip

  tags = var.default_tags
}