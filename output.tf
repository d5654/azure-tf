output "subnet_ids" {
  description = "IDs of the created subnets"
  value       = [azurerm_subnet.aci_snet[*].id, azurerm_subnet.appgw_snet[*].id]
}

output "subnet_cidrs_map" {
  description = "Map with names and CIDRs of the created subnets"
  value = [
    for snet in [azurerm_subnet.aci_snet, azurerm_subnet.appgw_snet] : {
      name             = snet[*].name
      address_prefixes = snet[*].address_prefixes
    }
  ]
}

output "aci_ids" {
  description = "Azure container instance group IDs"
  value       = [azurerm_container_group.main_aci[*].id]
}

output "aci_ip_addresses" {
  description = "IP addresses allocated to the container instance groups."
  value       = azurerm_container_group.main_aci[*].ip_address
}

output "aci_fqdns" {
  description = "The FQDNs of the container groups derived from `dns_name_label`."
  value       = azurerm_container_group.main_aci[*].fqdn
}