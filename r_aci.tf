################################
#   Azure Container Instance   #
################################
resource "azurerm_container_group" "main_aci" {
  count               = length(var.caf_naming)
  name                = var.aci_name
  location            = var.location
  resource_group_name = azurerm_resource_group.main_rg[count.index].name
  subnet_ids          = [azurerm_subnet.aci_snet[count.index].id]
  restart_policy      = var.restart_policy
  os_type             = var.os_type
  ip_address_type     = var.ip_address_type
  dns_name_label      = var.ip_address_type == "Private" ? null : var.aci_dns_name

  dynamic "container" {
    for_each = var.containers_config

    content {
      name   = container.key
      image  = var.container_images
      cpu    = container.value.cpu
      memory = container.value.memory

      environment_variables        = container.value.environment_variables
      secure_environment_variables = container.value.secure_environment_variables
      commands                     = container.value.commands

      dynamic "ports" {
        for_each = container.value.ports

        content {
          port     = ports.value.port
          protocol = ports.value.protocol
        }
      }
    }
  }

  tags = var.default_tags
}