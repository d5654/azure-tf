# since these variables are re-used - a locals block makes this more maintainable
locals {
  backend_address_pool_name      = "${azurerm_virtual_network.main_vnet[0].name}-beap"
  frontend_port_name             = "${azurerm_virtual_network.main_vnet[0].name}-feport"
  frontend_ip_configuration_name = "${azurerm_virtual_network.main_vnet[0].name}-feip"
  http_setting_name              = "${azurerm_virtual_network.main_vnet[0].name}-be-htst"
  listener_name                  = "${azurerm_virtual_network.main_vnet[0].name}-httplstn"
  request_routing_rule_name      = "${azurerm_virtual_network.main_vnet[0].name}-rqrt"
  redirect_configuration_name    = "${azurerm_virtual_network.main_vnet[0].name}-rdrcfg"
  gateway_ip_configuration_name  = "${azurerm_virtual_network.main_vnet[0].name}-gtw-pip"
}

resource "azurerm_application_gateway" "network" {
  count               = length(var.caf_naming)
  name                = azurecaf_name.caf_appgw[count.index].result
  resource_group_name = azurerm_resource_group.main_rg[count.index].name
  location            = var.location

  sku {
    name     = "Standard_v2"
    tier     = "Standard_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = local.gateway_ip_configuration_name
    subnet_id = azurerm_subnet.appgw_snet[count.index].id
  }

  frontend_port {
    name = local.frontend_port_name
    port = 80
  }

  frontend_ip_configuration {
    name                 = local.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.appgw_pip[count.index].id
  }

  dynamic "backend_address_pool" {
    for_each = azurerm_subnet.aci_snet[count.index].address_prefixes
    content {
      name         = local.backend_address_pool_name
      ip_addresses = [for i in range(4, 15) : cidrhost(backend_address_pool.value, i)]
    }
  }

  backend_http_settings {
    name                  = local.http_setting_name
    cookie_based_affinity = "Disabled"
    path                  = ""
    port                  = 80
    protocol              = "Http"
    request_timeout       = 60
  }

  http_listener {
    name                           = local.listener_name
    frontend_ip_configuration_name = local.frontend_ip_configuration_name
    frontend_port_name             = local.frontend_port_name
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = local.request_routing_rule_name
    priority                   = 9
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name
    backend_address_pool_name  = local.backend_address_pool_name
    backend_http_settings_name = local.http_setting_name
  }

  tags = var.default_tags

  depends_on = [azurerm_container_group.main_aci]
}