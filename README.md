## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azurecaf"></a> [azurecaf](#requirement\_azurecaf) | >=1.2.25 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >=3.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurecaf"></a> [azurecaf](#provider\_azurecaf) | 1.2.26 |
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 3.77.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurecaf_name.caf_appgw](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurecaf_name.caf_pip](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurecaf_name.caf_rg](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurecaf_name.caf_snet1](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurecaf_name.caf_snet2](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurecaf_name.caf_vnet](https://registry.terraform.io/providers/aztfmod/azurecaf/latest/docs/resources/name) | resource |
| [azurerm_application_gateway.network](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/application_gateway) | resource |
| [azurerm_container_group.main_aci](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/container_group) | resource |
| [azurerm_public_ip.appgw_pip](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip) | resource |
| [azurerm_resource_group.main_rg](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |
| [azurerm_subnet.aci_snet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_subnet.appgw_snet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) | resource |
| [azurerm_virtual_network.main_vnet](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aci_dns_name"></a> [aci\_dns\_name](#input\_aci\_dns\_name) | (Optional) The DNS label/name for the container group's IP. | `string` | n/a | yes |
| <a name="input_aci_name"></a> [aci\_name](#input\_aci\_name) | (Required) Specifies the name of the Container Group. | `string` | n/a | yes |
| <a name="input_aci_snet_svc_endpoints"></a> [aci\_snet\_svc\_endpoints](#input\_aci\_snet\_svc\_endpoints) | (Required) The address prefixes to use for the subnet. | `list(string)` | n/a | yes |
| <a name="input_allocation_method"></a> [allocation\_method](#input\_allocation\_method) | (Required) Defines the allocation method for this IP address. Possible values are `Static` or `Dynamic`. | `string` | n/a | yes |
| <a name="input_caf_naming"></a> [caf\_naming](#input\_caf\_naming) | (Required) Values to naming convention | <pre>list(object({<br>    name_rg     = string<br>    name_vnet   = string<br>    name_snet1  = string<br>    name_snet2  = string<br>    name_pip    = string<br>    name_appgw  = string<br>    suffixes_rg = string<br><br>    resource_type_rg    = string<br>    resource_type_vnet  = string<br>    resource_type_snet  = string<br>    resource_type_pip   = string<br>    resource_type_appgw = string<br>  }))</pre> | `[]` | no |
| <a name="input_container_images"></a> [container\_images](#input\_container\_images) | Container images | `string` | n/a | yes |
| <a name="input_containers_config"></a> [containers\_config](#input\_containers\_config) | Containers configurations. | <pre>map(object({<br>    cpu    = number<br>    memory = number<br><br>    environment_variables        = optional(map(string))<br>    secure_environment_variables = optional(map(string))<br>    commands                     = optional(list(string))<br><br>    ports = map(object({<br>      port     = number<br>      protocol = string<br>    }))<br>  }))</pre> | `{}` | no |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Tags to associate with your Azure Container Instances group. | `map(string)` | n/a | yes |
| <a name="input_domain_name_label"></a> [domain\_name\_label](#input\_domain\_name\_label) | (Optional) Label for the Domain Name. Will be used to make up the FQDN. If a domain name label is specified, an A DNS record is created for the public IP in the Microsoft Azure DNS system. | `string` | n/a | yes |
| <a name="input_ip_address_type"></a> [ip\_address\_type](#input\_ip\_address\_type) | (Optional) Specifies the IP address type of the container. `Public`, `Private` or `None`. | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | (Required) The location/region where the virtual network is created. | `string` | n/a | yes |
| <a name="input_os_type"></a> [os\_type](#input\_os\_type) | The OS for the container group. Allowed values are Linux and Windows. | `string` | n/a | yes |
| <a name="input_restart_policy"></a> [restart\_policy](#input\_restart\_policy) | Restart policy for the container group. Allowed values are `Always`, `Never`, `OnFailure`. | `string` | n/a | yes |
| <a name="input_sku_pip"></a> [sku\_pip](#input\_sku\_pip) | (Optional) The SKU of the Public IP. Accepted values are `Basic` and `Standard`. Defaults to `Basic`. | `string` | n/a | yes |
| <a name="input_vnet_address_space"></a> [vnet\_address\_space](#input\_vnet\_address\_space) | (Required) The address space that is used the virtual network. You can supply more than one address space. | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aci_fqdns"></a> [aci\_fqdns](#output\_aci\_fqdns) | The FQDNs of the container groups derived from `dns_name_label`. |
| <a name="output_aci_ids"></a> [aci\_ids](#output\_aci\_ids) | Azure container instance group IDs |
| <a name="output_aci_ip_addresses"></a> [aci\_ip\_addresses](#output\_aci\_ip\_addresses) | IP addresses allocated to the container instance groups. |
| <a name="output_subnet_cidrs_map"></a> [subnet\_cidrs\_map](#output\_subnet\_cidrs\_map) | Map with names and CIDRs of the created subnets |
| <a name="output_subnet_ids"></a> [subnet\_ids](#output\_subnet\_ids) | IDs of the created subnets |