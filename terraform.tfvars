#######################
#   Editable Values   #
##############################################
# ALL THIS BELOW VALUES CAN/SHOULD BE CHANGE #
##############################################

# Naming # Begin
caf_naming = [{
  name_rg    = "nucleo-devops"
  name_vnet  = "nucleo-devops"
  name_snet1 = "aci-001"
  name_snet2 = "appgw-001"
  name_pip   = "appgw-feip"
  name_appgw = "appgw-001"

  resource_type_rg    = "azurerm_resource_group"
  resource_type_vnet  = "azurerm_virtual_network"
  resource_type_snet  = "azurerm_subnet"
  resource_type_pip   = "azurerm_public_ip"
  resource_type_appgw = "azurerm_application_gateway"

  suffixes_rg = "wbnr-102023"
}]

location = "brazilsouth"
# Naming # End

# Network data # Begin
vnet_address_space = ["10.0.0.0/16"]

aci_snet_svc_endpoints = ["Microsoft.ContainerRegistry", "Microsoft.Storage"]
sku_pip                = "Standard"
allocation_method      = "Static"
domain_name_label      = "devops-core-wbnr-2023"
# Network data # End

# Container Instance # Begin
aci_name = "devops-core-wbnr-001" # Azure Container Instance Group name
os_type  = "Linux"

restart_policy  = "Always"           # Allowed values are `Always`, `Never`, `OnFailure`
ip_address_type = "Private"          # Allowed values are `Public`, `Private` or `None`
aci_dns_name    = "devops-core-wbnr" # FQDN only if the ip_address_type = Public

containers_config = {
  "devops-core-wbnr-001" = { # Container name
    cpu    = "0.5"
    memory = "1"

    environment_variables        = null # A list of environment variables to be set on the container
    secure_environment_variables = null # A list of sensitive environment variables to be set on the container
    commands                     = null # A list of commands which should be run on the container

    ports = {
      "http"  = { port = 80, protocol = "TCP" }
      "https" = { port = 443, protocol = "TCP" }
    }
  }
}
# Container Instance # End

# The Container Registry Credential, can be change
container_images = "mcr.microsoft.com/azuredocs/aci-helloworld:latest"

# Tags used to find this resource in the Azure dashboard
default_tags = {
  application = "hello-world"
  core        = "devops"
  environment = "webinar"
  billing     = "aubay"
}