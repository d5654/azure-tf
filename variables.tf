###############
#  Variables  #
###############

# This block must be immutable - BEGIN #
variable "client_id" {
  description = "(Required) Azure Service Principal Application ID."
  type        = string
}

variable "client_secret" {
  description = "(Required) Azure Service Principal Secret."
  type        = string
}

variable "tenant_id" {
  description = "(Required) Azure Tenant ID."
  type        = string
}

variable "subscription_id" {
  description = "(Required) Azure Subscription ID."
  type        = string
}
# This block must be immutable - END #
variable "caf_naming" {
  description = "(Required) Values to naming convention"
  type = list(object({
    name_rg     = string
    name_vnet   = string
    name_snet1  = string
    name_snet2  = string
    name_pip    = string
    name_appgw  = string
    suffixes_rg = string

    resource_type_rg    = string
    resource_type_vnet  = string
    resource_type_snet  = string
    resource_type_pip   = string
    resource_type_appgw = string
  }))
  default = []
}

variable "location" {
  description = "(Required) The location/region where the virtual network is created."
  type        = string
}

variable "vnet_address_space" {
  description = "(Required) The address space that is used the virtual network. You can supply more than one address space."
  type        = list(string)
}

variable "aci_snet_svc_endpoints" {
  description = "(Required) The address prefixes to use for the subnet."
  type        = list(string)
}

variable "sku_pip" {
  description = "(Optional) The SKU of the Public IP. Accepted values are `Basic` and `Standard`. Defaults to `Basic`."
  type        = string
}

variable "aci_name" {
  description = "(Required) Specifies the name of the Container Group."
  type        = string
}

variable "aci_dns_name" {
  description = "(Optional) The DNS label/name for the container group's IP."
  type        = string
}

variable "os_type" {
  description = "The OS for the container group. Allowed values are Linux and Windows."
  type        = string
}

variable "restart_policy" {
  description = "Restart policy for the container group. Allowed values are `Always`, `Never`, `OnFailure`."
  type        = string
}

variable "ip_address_type" {
  description = "(Optional) Specifies the IP address type of the container. `Public`, `Private` or `None`."
  type        = string
}

variable "allocation_method" {
  description = "(Required) Defines the allocation method for this IP address. Possible values are `Static` or `Dynamic`."
  type        = string
}

variable "domain_name_label" {
  description = "(Optional) Label for the Domain Name. Will be used to make up the FQDN. If a domain name label is specified, an A DNS record is created for the public IP in the Microsoft Azure DNS system."
  type        = string
}

variable "containers_config" {
  description = "Containers configurations."
  type = map(object({
    cpu    = number
    memory = number

    environment_variables        = optional(map(string))
    secure_environment_variables = optional(map(string))
    commands                     = optional(list(string))

    ports = map(object({
      port     = number
      protocol = string
    }))
  }))
  default = {}
}

variable "container_images" {
  description = "Container images"
  type        = string
}

variable "default_tags" {
  description = "Tags to associate with your Azure Container Instances group."
  type        = map(string)
}