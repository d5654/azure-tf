resource "azurecaf_name" "caf_rg" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_rg
  resource_type = var.caf_naming[count.index].resource_type_rg
  suffixes      = [var.caf_naming[count.index].suffixes_rg]
  clean_input   = true
}

resource "azurecaf_name" "caf_vnet" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_vnet
  resource_type = var.caf_naming[count.index].resource_type_vnet
  clean_input   = true
}

resource "azurecaf_name" "caf_snet1" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_snet1
  resource_type = var.caf_naming[count.index].resource_type_snet
  clean_input   = true
}

resource "azurecaf_name" "caf_snet2" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_snet2
  resource_type = var.caf_naming[count.index].resource_type_snet
  clean_input   = true
}

resource "azurecaf_name" "caf_pip" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_pip
  resource_type = var.caf_naming[count.index].resource_type_pip
  clean_input   = true
}

resource "azurecaf_name" "caf_appgw" {
  count = length(var.caf_naming)

  name          = var.caf_naming[count.index].name_appgw
  resource_type = var.caf_naming[count.index].resource_type_appgw
  clean_input   = true
}