# Define the name of your resource group and region
resource "azurerm_resource_group" "main_rg" {
  count    = length(var.caf_naming)
  name     = azurecaf_name.caf_rg[count.index].result
  location = var.location
  tags     = var.default_tags
}