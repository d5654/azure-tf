# Strongly recommend using the required_providers block to set the
# Azure Provider source and version being used
terraform {
  backend "http" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
    azurecaf = {
      source  = "aztfmod/azurecaf"
      version = ">=1.2.25"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}

  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  subscription_id = var.subscription_id
}